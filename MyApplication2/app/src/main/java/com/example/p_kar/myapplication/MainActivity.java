package com.example.p_kar.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    //method connected with starting activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //git master
        setupUI();
    }

    private void setupUI() {
        Button first = findViewById(R.id.first_button);
        Button second = findViewById(R.id.sec_button);

//        first.setText("Czesław");
//          Toast.makeText(MainActivity.this,
//                        "Nie ma złota, Biedaku!",
//                        Toast.LENGTH_LONG).show();

        first.setOnClickListener(v -> {
            Intent intent =
                    new Intent(MainActivity.this,
                            GoldenActivity.class);

            startActivity(intent);
        });

        second.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this,
                    CurrencyActivity.class);
            startActivity(intent);
        });
    }
}
