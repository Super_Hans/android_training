package com.example.p_kar.myapplication;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.p_kar.myapplication.models.Rate;

import java.util.List;

public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.Row> {

    private List<Rate> rates;

    CurrencyAdapter(List<Rate> rates) {
        this.rates = rates;
    }

    @NonNull
    @Override
    public Row onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.row_currency, viewGroup, false);

        return new Row(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Row row, int i) {
        // we take rate from certain position
        Rate rate = rates.get(i);

        row.bind(rate);
    }

    @Override
    public int getItemCount() {
        //if 0 elements - > than work does not start
        return rates.size();
    }

    class Row extends RecyclerView.ViewHolder {
        //elements of row
        private TextView name;
        private TextView description;
        private TextView mid;


        Row(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name_textView);
            description = itemView.findViewById(R.id.description);
            mid = itemView.findViewById(R.id.mid);
        }

        // method that fills rows with specified rate
        void bind(Rate rate) {
            name.setText(rate.getCode());
            description.setText(rate.getName());
            mid.setText(String.format("%f", rate.getMid()));
        }
    }
}
