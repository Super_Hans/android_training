package com.example.p_kar.myapplication;

import com.example.p_kar.myapplication.models.Gold;
import com.example.p_kar.myapplication.models.Table;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiNBP {

    String BASE_URL = "http://api.nbp.pl/api/";

    @GET("exchangerates/tables/A")
    Call<List<Table>> getCurrencies();

    @GET("cenyzlota/last/{number}")
    Call<List<Gold>> getGold(@Path("number") int numberOfElements);

    //getting api from net
    static ApiNBP getApi() {
        return ApiNBP.getRetrofit().create(ApiNBP.class);
    }

    static Retrofit getRetrofit() {
        //adding logs for download data
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        // object for downloading data
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();

        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }
}
