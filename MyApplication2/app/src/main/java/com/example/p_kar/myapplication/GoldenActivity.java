package com.example.p_kar.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.p_kar.myapplication.models.Gold;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GoldenActivity extends Activity {

    private List<Integer> days = Arrays.asList(10, 20, 30, 50, 90);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_gold);

        setSpinner();
    }

    private void setSpinner() {
        Spinner spinner = findViewById(R.id.spinner);
        // throws list of options in dropbox
        ArrayAdapter<Integer> adapter =
                new ArrayAdapter<>(this,
                        android.R.layout.simple_spinner_item,
                        days);

        spinner.setAdapter(adapter);

        //adding reactions for choosing elements from spinner
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                fetchData(days.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void fetchData(Integer numberOfDays) {
        Call<List<Gold>> call = ApiNBP.getApi().getGold(numberOfDays);
        call.enqueue(getCallback());
    }

    private Callback<List<Gold>> getCallback() {
        return new Callback<List<Gold>>() {
            @Override
            public void onResponse(Call<List<Gold>> call, Response<List<Gold>> response) {
                if (response.body() != null && !response.body().isEmpty());
                showData(response.body());
            }

            @Override
            public void onFailure(Call<List<Gold>> call, Throwable t) {
                t.printStackTrace();
            }
        };
    }

    private void showData(List<Gold> goldList) {
        LineChart chart = findViewById(R.id.chart);

        chart.clear();

        ArrayList<Entry> values = new ArrayList<>();

        for (int position = 0; position < goldList.size(); position++) {
            Entry entry = new Entry(position,
                    goldList.get(position).getPrice());

            values.add(entry);
        }

        LineDataSet set = new LineDataSet(values, "Gold Exchange");
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setColor(getResources().getColor(R.color.colorPrimaryDark));
        set.setFillColor(getResources().getColor(R.color.colorPrimary));
        set.setDrawFilled(true);
        set.setDrawCircles(false);

        set.setLineWidth(2f);

        LineData data = new LineData(set);
        data.setValueTextSize(10f);

        chart.setData(data);
        chart.invalidate();
    }

}
