package com.example.p_kar.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.p_kar.myapplication.models.Rate;
import com.example.p_kar.myapplication.models.Table;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrencyActivity extends Activity {
    ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // fetch data from act currency
        setContentView(R.layout.act_currency);

        fetchData();

        // setting toolbar (upper)
        setToolbar();
        progressBar = findViewById(R.id.progress_circle);
    }

    private void setToolbar() {
        if (getActionBar() != null) {
            //if exists - adding title (whatever you want)
            getActionBar().setTitle("Foreign Exchange");
            // maintenance of arrows
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // service of upper arrow in menu
        if (item.getItemId() == android.R.id.home) {
            //closing of current activity
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void fetchData() {
        Call<List<Table>> call = ApiNBP.getApi().getCurrencies();
        // new thread
        call.enqueue(getCallback());

    }

    private Callback<List<Table>> getCallback() {
        return new Callback<List<Table>>() {
            @Override
            public void onResponse(Call<List<Table>> call, Response<List<Table>> response) {

                progressBar.setVisibility(View.GONE);
                // if range is fine with range of codes 200-299
                if (response.isSuccessful()
                        && !(response.body() != null
                        && response.body().isEmpty())) {

                    // displaying data
                    showData(response.body().get(0));

                }

            }

            @Override
            public void onFailure(Call<List<Table>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);

                t.printStackTrace();
                String message = t.getMessage();
//
//                Toast.makeText(
//                        CurrencyActivity.this,
//                        message,
//                        Toast.LENGTH_LONG)
//                        .show();
                // filling and display of errors
                TextView error = findViewById(R.id.error);
                error.setText(message);
                error.setVisibility(View.VISIBLE);
            }
        };
    }

    private void showData(Table table) {
//        for (Rate rate : table.getRates()) {
//            System.out.println(rate);
        RecyclerView view = findViewById(R.id.recycler_view);

        CurrencyAdapter adapter = new CurrencyAdapter(table.getRates());
        view.setAdapter(adapter);

        view.setLayoutManager(new LinearLayoutManager(this));

        DividerItemDecoration decoration =
                new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);

        view.addItemDecoration(decoration);

    }

}
